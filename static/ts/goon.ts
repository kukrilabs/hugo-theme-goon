import GAnalytics from "ganalytics";

declare const gaTrackingCode: string;

const ga = GAnalytics(gaTrackingCode, { aid: '1' });

function attachGAnalyticsListeners() {
    console.log(ga);
};

document.onload = () => {
    attachGAnalyticsListeners();
};
