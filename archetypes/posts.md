---
title: "{{ replace .Name "-" " " | title }}"
description: Description
date: {{ .Date }}
draft: true
toc: true

featuredimage:
    url: /img/placeholder-3x2.png
    svg: null
    alt: 
    # One of the Bulma image ratios
    # https://bulma.io/documentation/elements/image/
    ratio: 

intro: Intro text
preamble: |
    Preamble text
---
