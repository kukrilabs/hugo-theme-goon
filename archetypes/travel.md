---
title: "{{ replace .Name "-" " " | title }}"
description: 
date: {{ .Date }}
draft: true
toc: false

type: travel
# Can be "set", "None"
#layout: set

location:
    title: London, UK
    lat: 
    lon: 

coverimage:
    ratio: square # Match Bulma image ratios
    url: /img/placeholder-1x1.png
    alt: "{{ replace .Name "-" " " | title }}"

stats:
    - value: 1000
      unit: photos taken
    - value: 20
      unit: days
---
