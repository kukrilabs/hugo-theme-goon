# Goon

Based on [HTML5 Boilerplate v7.2.0](https://html5boilerplate.com/), follows best practices to create a fast, lightweight theme for [Hugo](https://gohugo.io/).

This work was based on the excellent designs of [`@stammy`](https://paulstamatiou.com/) from Twitter.

## Archetypes

### `travel/set`

### `gear`

## Resources

- [`HEAD` - A list of everything that could go into the `<head>`](https://github.com/joshbuchea/HEAD)
- [Front-end Checklist](https://github.com/thedaviddias/Front-End-Checklist)

## In-Use

This theme is currently in use at:

- [ewjo.co](https://ewjo.co) - Personal blog of Ewan Jones
